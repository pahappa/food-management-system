/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pahappa.systems.models;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.sers.webutils.model.BaseEntity;
import org.sers.webutils.model.security.User;


@Entity
@Table(name = "account")
public class Account extends BaseEntity{
    
    private double outstandingBalance;
    private double workingDays;
    private User user;
    private Set<Requisition> requistion;

    @OneToMany(mappedBy = "accountId")
    public Set<Requisition> getRequistion() {
        return requistion;
    }

    
    @Column(name = "user_id")
    @OneToOne(targetEntity = User.class)
    public User getUser() {
        return user;
    }


    @Column(name = "outstanding_balance")
    public double getOutstandingBalance() {
        return outstandingBalance;
    }

    @Column(name = "working_days")
    public double getWorkingDays() {
        return workingDays;
    }

    
    public void setRequistion(Set<Requisition> requistion) {
        this.requistion = requistion;
    }
     
    public void setUser(User user) {
        this.user = user;
    }

    public void setOutstandingBalance(double outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public void setWorkingDays(double workingDays) {
        this.workingDays = workingDays;
    }
    
    
}
