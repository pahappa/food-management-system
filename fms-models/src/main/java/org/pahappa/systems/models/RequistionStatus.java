package org.pahappa.systems.models;

public enum RequistionStatus {
    
    PENDING, APPROVED, ACKNOWLEDGED, REJECTED
}
