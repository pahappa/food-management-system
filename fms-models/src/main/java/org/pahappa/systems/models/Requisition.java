package org.pahappa.systems.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.sers.webutils.model.BaseEntity;

@Entity
@Table(name = "requisition")
public class Requisition extends BaseEntity {

        private int amountRequested;
        private int daysRequested;
        private String comment;
        private Date statusUpdatedDate;
        private Account accountId;
        private RequistionStatus requisitionStatus;

        @ManyToOne(targetEntity = Account.class)
        @Column(name = "account_id")
        public Account getAccountId() {
                return accountId;
        }

        @Column(name = "amount_requested", nullable = false, length = 11)
        public int getAmountRequested() {
                return amountRequested;
        }

        @Column(name = "days_requested", nullable = false)
        public int getDaysRequested() {
                return daysRequested;
        }

        @Column(name = "comment", length = 1000, nullable = true)
        public String getComment() {
                return comment;
        }

        @Temporal(TemporalType.TIMESTAMP)
        @Column(name = "status_updated_date", nullable = false)
        public Date getStatusUpdatedDate() {
                return statusUpdatedDate;
        }

        @Enumerated(EnumType.ORDINAL)
        @Column(name = "status", nullable = false)
        public RequistionStatus getRequisition_status() {
                return requisitionStatus;
        }

        public void setAmountRequested(int amountRequested) {
                this.amountRequested = amountRequested;
        }

        public void setDaysRequested(int daysRequested) {
                this.daysRequested = daysRequested;
        }

        public void setComment(String comment) {
                this.comment = comment;
        }

        public void setStatusUpdatedDate(Date statusUpdatedDate) {
                this.statusUpdatedDate = statusUpdatedDate;
        }

        public void setAccountId(Account accountId) {
                this.accountId = accountId;
        }

        public void setRequisition_status(RequistionStatus requisitionStatus) {
                this.requisitionStatus = requisitionStatus;
        }
}
